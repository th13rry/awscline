# Miscellaneus Scripts

## Cwatch
A script to execute a cloudwatch query using an aws profile, a
selected log-group and in a selected time frame. If no file containing the 
query to execute is provided, it will it read from stdin.

This script offers various options to preselect some parameters, alternatively
you can let `fzf` guide you in the selection :

  - **aws profile** : if not provided, the script will extract all configured
  profile from your `AWS_CONFIG_FILE` and provide it to `fzf` for you 
  to select one out of them
  - **log group** : if not provided, the script will fetch all available
  log groups from the selected **aws profile** using the `describe-log-groups`
  api of `aws`

### Requirements
**This scripts requires the following binaries to work :**
- **[fzf](https://github.com/junegunn/fzf)**
- **[jq](https://github.com/jqlang/jq)**
- **[aws](
https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html
)**


```
Usage:
  cwatch [options]

OPTIONS
  -f,--file        Input file containing the query
  -n,--no-profile  Disable profile selection, use current AWS_PROFILE
  -p,--profile     Profile to use for this query
  -l,--log-group   Log group to run the query on
  -t,--time        Time frame of the query [number](m|h|h|d|w)
                   defaults to 1d
                   m : between now and [number] minute ago
                   h : between now and [number] hour ago
                   d : between now and [number] day ago
                   w : between now and [number] week ago
  -h,--help        Show list of command line options

```

## Wafck
A script to analyze the result of a cloudwatch query to get waf
blocked request, with at least these to informations :

  - `terminatingRuleId` : the rule that ended the request
  - `httpRequest.headers.0.value` : most of the time, the Host header from
  the blocked request

This scripts mostly helps us detecting legitimate traffic that is blocked
by the waf, and needs an exception in the `network.yml` configuration  file
of the appropriate environment in [udm-infra](
https://gitlab.com/ratp-usine-digitale/group-ul/ul-infra/udm/udm-infra
)

For an example on how to add and exception see [MR 243] on that repo.

If no file containing the results to analyze is provided, it will it read
from stdin.

### Requirements
**This scripts requires the following binaries to work :**
- **[jq](https://github.com/jqlang/jq)**


```
Usage:
  wafck [options]

OPTIONS
  -f,--file        Input file containing the results to analyze
  -h,--help        Show list of command line options

```
